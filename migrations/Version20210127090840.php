<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210127090840 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE factor ADD created_at DATETIME NOT NULL, ADD updated_at DATETIME NOT NULL, CHANGE status status TINYINT(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE `order` ADD created_at DATETIME NOT NULL, ADD updated_at DATETIME NOT NULL, CHANGE status status TINYINT(1) DEFAULT NULL, CHANGE quantity quantity INT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE factor DROP created_at, DROP updated_at, CHANGE status status TINYINT(1) DEFAULT \'0\'');
        $this->addSql('ALTER TABLE `order` DROP created_at, DROP updated_at, CHANGE status status TINYINT(1) DEFAULT \'0\', CHANGE quantity quantity INT DEFAULT 1');
    }
}
