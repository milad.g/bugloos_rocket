<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210126150038 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE factor (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, factor_number VARCHAR(255) DEFAULT NULL, total_price VARCHAR(255) DEFAULT NULL, total_discount VARCHAR(255) DEFAULT NULL, total_final_price VARCHAR(255) DEFAULT NULL, status TINYINT(1) DEFAULT 0, INDEX IDX_ED38EC00A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `order` (id INT AUTO_INCREMENT NOT NULL, product_id INT DEFAULT NULL, factor_id INT DEFAULT NULL, user_id INT DEFAULT NULL, price VARCHAR(255) DEFAULT NULL, discount VARCHAR(255) DEFAULT NULL, final_price VARCHAR(255) DEFAULT NULL, status TINYINT(1) DEFAULT 0, quantity INT DEFAULT 1, INDEX IDX_F52993984584665A (product_id), INDEX IDX_F5299398BC88C1A3 (factor_id), INDEX IDX_F5299398A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE factor ADD CONSTRAINT FK_ED38EC00A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE `order` ADD CONSTRAINT FK_F52993984584665A FOREIGN KEY (product_id) REFERENCES product (id)');
        $this->addSql('ALTER TABLE `order` ADD CONSTRAINT FK_F5299398BC88C1A3 FOREIGN KEY (factor_id) REFERENCES factor (id)');
        $this->addSql('ALTER TABLE `order` ADD CONSTRAINT FK_F5299398A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE product CHANGE stock stock INT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE `order` DROP FOREIGN KEY FK_F5299398BC88C1A3');
        $this->addSql('DROP TABLE factor');
        $this->addSql('DROP TABLE `order`');
        $this->addSql('ALTER TABLE product CHANGE stock stock INT DEFAULT 0');
    }
}
