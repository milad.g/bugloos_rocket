<?php

namespace App\Entity;

use App\Repository\FactorRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass=FactorRepository::class)
 */
class Factor
{
    use TimestampableEntity;
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $factorNumber;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $totalPrice;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $totalDiscount;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $totalFinalPrice;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $status = 0;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="factors")
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity=Order::class, mappedBy="factor", fetch="EXTRA_LAZY")
     */
    private $orders;

    public function __construct()
    {
        $this->orders = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFactorNumber(): ?string
    {
        return $this->factorNumber;
    }

    public function setFactorNumber(?string $factorNumber): self
    {
        $this->factorNumber = $factorNumber;

        return $this;
    }

    public function getTotalPrice(): ?string
    {
        return $this->totalPrice;
    }

    public function setTotalPrice(?string $totalPrice): self
    {
        $this->totalPrice = $totalPrice;

        return $this;
    }

    public function getTotalDiscount(): ?string
    {
        return $this->totalDiscount;
    }

    public function setTotalDiscount(?string $totalDiscount): self
    {
        $this->totalDiscount = $totalDiscount;

        return $this;
    }

    public function getTotalFinalPrice(): ?string
    {
        return $this->totalFinalPrice;
    }

    public function setTotalFinalPrice(?string $totalFinalPrice): self
    {
        $this->totalFinalPrice = $totalFinalPrice;

        return $this;
    }

    public function getStatus(): ?bool
    {
        return $this->status;
    }

    public function setStatus(?bool $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|Order[]
     */
    public function getOrders(): Collection
    {
        return $this->orders;
    }

    public function addOrder(Order $order): self
    {
        if (!$this->orders->contains($order)) {
            $this->orders[] = $order;
            $order->setFactor($this);
        }

        return $this;
    }

    public function removeOrder(Order $order): self
    {
        if ($this->orders->removeElement($order)) {
            // set the owning side to null (unless already changed)
            if ($order->getFactor() === $this) {
                $order->setFactor(null);
            }
        }

        return $this;
    }
}
