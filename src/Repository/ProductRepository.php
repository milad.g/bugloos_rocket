<?php

namespace App\Repository;

use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    public function __construct(ManagerRegistry $registry, CategoryRepository $categoryRepository)
    {
        parent::__construct($registry, Product::class);
        $this->categoryRepository = $categoryRepository;
    }


    /**
     * @param $query
     * @return Query
     */
    public function findLatestProductsWithQuery($query)
    {
        return $this->createQueryBuilder('p')
            ->andWhere("p.title LIKE :term OR p.description LIKE :term")
            ->setParameter('term', '%'.$query.'%')
            ->orderBy('p.createdAt','DESC')
            ->getQuery()
        ;
    }

    /**
     * @param $limit
     * @return int|mixed|string
     */
    public function findMostViewedProducts($limit)
    {
        return $this->createQueryBuilder('p')
            ->orderBy('p.views','DESC')
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult()
            ;
    }

    /**
     * @param $limit
     * @return int|mixed|string
     */
    public function findLatestProducts($limit)
    {
        return $this->createQueryBuilder('p')
            ->orderBy('p.createdAt','DESC')
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @param $limit
     * @return int|mixed|string
     */
    public function findSliderProducts($limit)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.slider IS NOT NULL')
            ->orderBy('p.createdAt','DESC')
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @param $limit
     * @return int|mixed|string
     */
    public function findProductsRandomly($limit)
    {
        return $this->createQueryBuilder('p')
            ->addSelect('RAND() as HIDDEN rand')
            ->orderBy('rand')
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @param $category
     * @param $search
     * @param $order
     * @param $sortItems
     * @return Query
     */
    public function findProductsByShopFilter($category, $search, $order, $sortItems)
    {
        $query = $this->createQueryBuilder('p');

        if($category){
            $category = $this->categoryRepository->findOneBy(['slug' => $category]);
            $query = $query->andWhere('p.category = :val')->setParameter('val',$category);
        }

        if($search){
            $query = $query->andWhere('p.title LIKE :search OR p.description LIKE :search')->setParameter('search','%'.$search.'%');
        }

        if($order && array_key_exists($order, $sortItems)){
            $field = $sortItems[$order]['field'];
            $type = $sortItems[$order]['type'];
            $query = $query->orderBy('p.'.$field,$type);
        }

        return $query->getQuery();
    }

    public function findRelativeProducts($product)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.category = :val')
            ->setParameter('val',$product->getCategory())
            ->andWhere('p.id != :id')
            ->setParameter('id', $product->getId())
            ->setMaxResults(4)
            ->getQuery()
            ->getResult()
        ;
    }


    // /**
    //  * @return Product[] Returns an array of Product objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Product
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
