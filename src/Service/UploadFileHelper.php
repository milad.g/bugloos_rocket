<?php


namespace App\Service;


use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\String\Slugger\SluggerInterface;

class UploadFileHelper
{
    private $slugger;
    private $kernel;

    public function __construct(SluggerInterface $slugger, KernelInterface $kernel)
    {
        $this->slugger = $slugger;
        $this->kernel = $kernel;
    }

    public function uploadFile($file)
    {
        $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        // this is needed to safely include the file name as part of the URL
        $safeFilename = $this->slugger->slug($originalFilename);
        $newFilename = $safeFilename . '-' . uniqid() . '.' . $file->guessExtension();
        $directory = $this->kernel->getProjectDir() . '/public/uploads';

        // Move the file
        try {
            $file->move($directory, $newFilename);
        } catch (FileException $e) {
            throw new FileException("The file could not be uploaded");
        }

        return $newFilename;
    }
}