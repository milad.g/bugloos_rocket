<?php


namespace App\EventListener;

use App\Repository\OrderRepository;
use Symfony\Component\Security\Core\Security;
use Twig\Environment;

class ControllerListener
{
    private $twig;
    private $security;
    private $orderRepository;

    public function __construct(Environment $twig, Security $security, OrderRepository $orderRepository)
    {
        $this->twig     = $twig;
        $this->security = $security;
        $this->orderRepository = $orderRepository;
    }

    public function onKernelController()
    {
        $user = $this->security->getUser();
        if($user){
            $orders = $this->orderRepository->findBasketOrderByUser($user);
            $this->twig->addGlobal( 'globalOrders', $orders );
        }
    }
}