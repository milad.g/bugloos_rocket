<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class CommentController extends AbstractController
{
    private $csrfTokenManager;

    public function __construct(CsrfTokenManagerInterface $csrfTokenManager)
    {
        $this->csrfTokenManager = $csrfTokenManager;
    }

    /**
     * @Route("/comment/store", name="app_comment_store")
     */
    public function store(Request $request, ProductRepository $productRepository, EntityManagerInterface $em, ValidatorInterface $validator): Response
    {
        $token = new CsrfToken('add-comment', $request->request->get('_csrf_token'));
        if (!$this->csrfTokenManager->isTokenValid($token)) {
            throw new InvalidCsrfTokenException();
        }

        $product = $productRepository->findOneBy(['id' => $request->request->get('product')]);
        if(!$product){
            throw new NotFoundHttpException("This product is not available");
        }

        $comment = new Comment();

        $comment->setText($request->request->get('text'));
        $comment->setProduct($product);
        $comment->setUser($this->getUser());

        $errors = $validator->validate($comment);

        if (count($errors) > 0) {
            foreach ($errors as $error){
                $this->addFlash("error",$error->getMessage());
            }

            return $this->redirectToRoute("app_product_show",['slug' => $product->getSlug()]);
        }

        $em->persist($comment);
        $em->flush();

        $this->addFlash("success","Comment added successfully");

        return $this->redirectToRoute("app_product_show",['slug' => $product->getSlug()]);
    }
}
