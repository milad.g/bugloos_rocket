<?php

namespace App\Controller;

use App\Repository\ProductRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="app_home_index")
     */
    public function index(ProductRepository $productRepository): Response
    {
        $mostViewedProducts = $productRepository->findMostViewedProducts(5);
        $latestProducts = $productRepository->findLatestProducts(5);
        $sliderProducts = $productRepository->findSliderProducts(3);

        $data = [
            'mostViewedProducts' => $mostViewedProducts,
            'latestProducts' => $latestProducts,
            'sliderProducts' => $sliderProducts,
        ];

        return $this->render('home/index.html.twig', $data);
    }
}
