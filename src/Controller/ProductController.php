<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Product;
use App\Repository\CategoryRepository;
use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProductController extends AbstractController
{
    /**
     * @Route("/products", name="app_product_index")
     */
    public function index(Request $request, CategoryRepository $categoryRepository, ProductRepository $productRepository, PaginatorInterface $paginator): Response
    {
        $categories = $categoryRepository->findSortedByTitleCategoriesWithProducts();
        $randomProducts = $productRepository->findProductsRandomly(2);

        $sortItems = [
            'popularity' => ['title' => 'Sort by popularity', 'field' => 'views', 'type' => 'DESC'],
            'price-low-to-high' => ['title' => 'Sort by price: low to high', 'field' => 'finalPrice', 'type' => 'DESC'],
            'price-high-to-low' => ['title' => 'Sort by price: high to low', 'field' => 'finalPrice', 'type' => 'ASC'],
            'latest' => ['title' => 'Sort by latest', 'field' => 'createdAt', 'type' => 'DESC'],
            'oldest' => ['title' => 'Sort by oldest', 'field' => 'createdAt', 'type' => 'ASC'],
        ];

        $category = $request->query->get('category');
        $search = $request->query->get('search');
        $order = $request->query->get('order');

        $itemsPerPage = 3;
        $pagination = $paginator->paginate(
            $productRepository->findProductsByShopFilter($category, $search, $order, $sortItems),
            $request->query->getInt('page', 1),
            $itemsPerPage
        );

        $paginationFrom = ($pagination->getCurrentPageNumber() - 1) * $itemsPerPage + 1;
        $paginationTo = $pagination->getCurrentPageNumber() * $itemsPerPage;

        $items = [
            "title" => "Products",
            "breadcrumbs" => [
                ['title' => "Products", 'link' => "app_product_index"]
            ],
            "categories" => $categories,
            "randomProducts" => $randomProducts,
            "pagination" => $pagination,
            "paginationFrom" => $paginationFrom,
            "paginationTo" => $paginationTo,
            "sortItems" => $sortItems,
        ];

        return $this->render('pages/product/index.html.twig', $items);
    }

    /**
     * @Route("/products/{slug}", name="app_product_show")
     */
    public function show(Request $request, Product $product, ProductRepository $productRepository, EntityManagerInterface $em)
    {
        $relatedProducts = $productRepository->findRelativeProducts($product);

        $cookie = $request->cookies->get('view_product_'.$product->getId());
        if(!$cookie){
            $time = time() + 3600;
            $cookie = new Cookie("view_product_".$product->getId(),"You view this product",$time);
            $response = new Response();
            $response->headers->setCookie($cookie);
            $response->send();

            $product->setViews($product->getViews() + 1);
            $em->flush();
        }

        $items = [
            "title" => "Products",
            "breadcrumbs" => [
                ['title' => "Products", 'link' => "app_product_index"],
                ['title' => $product->getTitle(), 'link' => "#"]
            ],
            "product" => $product,
            "relatedProducts" => $relatedProducts,
        ];

        return $this->render('pages/product/show.html.twig', $items);
    }
}
