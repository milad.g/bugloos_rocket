<?php

namespace App\Controller;

use App\Entity\Factor;
use App\Entity\Order;
use App\Message\AddToCart;
use App\Repository\FactorRepository;
use App\Repository\OrderRepository;
use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

/**
 * Class CartController
 * @package App\Controller
 * @IsGranted("ROLE_USER")
 */
class CartController extends AbstractController
{
    private $csrfTokenManager;

    public function __construct(CsrfTokenManagerInterface $csrfTokenManager)
    {
        $this->csrfTokenManager = $csrfTokenManager;
    }

    /**
     * @Route("/cart/order/store", name="app_cart_order_store", methods={"POST"})
     */
    public function store(Request $request, MessageBusInterface $messageBus): Response
    {
        $addToCart = new AddToCart($request->request->get('_csrf_token'), $request->request->get('product'));
        $messageBus->dispatch($addToCart);

        $this->addFlash("success","Product added to your cart successfully");

        return $this->redirectToRoute("app_cart_order");
    }

    /**
     * @Route("/cart/order", name="app_cart_order")
     */
    public function order(OrderRepository $orderRepository, FactorRepository $factorRepository, EntityManagerInterface $entityManager)
    {
        $orders = $orderRepository->findBasketOrderByUser($this->getUser());

        if($orders){
            foreach ($orders as $order) {
                $order->setFactor(null);
            }

            $lastFactor = $factorRepository->findUnFinishedFactorByUser($this->getUser());
            if ($lastFactor) {
                $entityManager->remove($lastFactor);
            }
            $entityManager->flush();
        }

        $items = [
            "title" => "Shopping Cart",
            "breadcrumbs" => [
                ['title' => "Shopping Cart", 'link' => "#"],
            ],
            "orders" => $orders,
        ];

        return $this->render('pages/cart/orders.html.twig', $items);
    }

    /**
     * @Route("/cart/checkout", name="app_cart_checkout")
     */
    public function checkOut(OrderRepository $orderRepository, EntityManagerInterface $entityManager, FactorRepository $factorRepository)
    {
        $orders = $orderRepository->findBasketOrderByUser($this->getUser());
        if($orders) {
            $totalPrice = 0;
            $totalFinalPrice = 0;
            foreach ($orders as $order) {
                $totalPrice += $order->getPrice();
                $totalFinalPrice += $order->getFinalPrice();
                $order->setFactor(null);
            }

            $lastFactor = $factorRepository->findUnFinishedFactorByUser($this->getUser());
            if ($lastFactor) {
                $entityManager->remove($lastFactor);
                $entityManager->flush();
            }

            $factor = new Factor();
            $factor->setUser($this->getUser());
            $factor->setFactorNumber("BGL-" . time() . rand(111, 999));
            $factor->setTotalPrice($totalPrice);
            $factor->setTotalDiscount($totalPrice - $totalFinalPrice);
            $factor->setTotalFinalPrice($totalFinalPrice);
            $entityManager->persist($factor);

            foreach ($orders as $order) {
                $order->setFactor($factor);
            }

            $entityManager->flush();

            $items = [
                "title" => "Checkout",
                "breadcrumbs" => [
                    ['title' => "Shopping Cart", 'link' => "app_cart_order"],
                    ['title' => "Checkout", 'link' => "#"],
                ],
                "orders" => $orders,
                "factor" => $factor,
            ];

            return $this->render('pages/cart/checkout.html.twig', $items);
        } else {
            return $this->redirectToRoute("app_cart_order");
        }
    }

    /**
     * @Route("/cart/order/{id}/delete", name="app_cart_order_delete")
     */
    public function delete(Order $order, EntityManagerInterface $entityManager, OrderRepository $orderRepository)
    {
        $entityManager->remove($order);
        $entityManager->flush();

        $orders = $orderRepository->findBasketOrderByUserCount($this->getUser());

        return $this->json([
            'orders' => $orders
        ]);
    }

    /**
     * @Route("/cart/payment", name="app_cart_payment")
     */
    public function payment(OrderRepository $orderRepository, FactorRepository $factorRepository, EntityManagerInterface $entityManager)
    {
        $orders = $orderRepository->findBasketOrderByUser($this->getUser());
        $factor = $factorRepository->findUnFinishedFactorByUser($this->getUser());

        if($orders && $factor){
            foreach ($orders as $order) {
                $order->getProduct()->setStock($order->getProduct()->getStock() - 1);
                $order->setStatus(1);
            }
            $factor->setStatus(1);

            $entityManager->flush();


            $items = [
                "title" => "Checkout",
                "breadcrumbs" => [
                    ['title' => "Shopping Cart", 'link' => "app_cart_order"],
                    ['title' => "Checkout", 'link' => "#"],
                ],
                "orders" => $orders,
                "factor" => $factor,
            ];

            return $this->render('pages/cart/payment.html.twig', $items);
        } else {
            return $this->redirectToRoute("app_cart_order");
        }
    }
}
