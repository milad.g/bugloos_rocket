<?php

namespace App\Controller;

use App\Entity\Factor;
use App\Entity\User;
use App\Form\ProfileFormType;
use App\Repository\FactorRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

/**
 * Class UserController
 * @package App\Controller
 * @IsGranted("ROLE_USER")
 */
class UserController extends AbstractController
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    /**
     * @Route("/user/dashboard", name="app_user_dashboard")
     */
    public function dashboard()
    {
        $items = [
            "title" => "Dashboard",
            "breadcrumbs" => [
                ['title' => "Dashboard", 'link' => "#"]
            ],
        ];

        return $this->render('pages/user/dashboard.html.twig', $items);
    }

    /**
     * @Route("/user/profile", name="app_user_profile")
     */
    public function profile(Request $request): Response
    {
        $user = $this->security->getUser();
        $form = $this->createForm(ProfileFormType::class, $user);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            $this->addFlash("success","Your profile updated successfully!");
        }


        $items = [
            "title" => "Profile",
            "breadcrumbs" => [
                ['title' => "Profile", 'link' => "#"]
            ],
            'profileForm' => $form->createView()
        ];

        return $this->render('pages/user/profile.html.twig', $items);
    }

    /**
     * @Route("/user/factors", name="app_user_factors")
     */
    public function factors(FactorRepository $factorRepository)
    {
        $factors = $factorRepository->findFinishedFactorsByUser($this->getUser());

        $items = [
            "title" => "Factors",
            "breadcrumbs" => [
                ['title' => "Factors", 'link' => "#"]
            ],
            "factors" => $factors,
        ];

        return $this->render('pages/user/factors.html.twig', $items);
    }

    /**
     * @Route("/user/factors/{id}/orders", name="app_user_factor_orders")
     */
    public function orders(Factor $factor)
    {
        $items = [
            "title" => "Orders for factor ".$factor->getFactorNumber(),
            "breadcrumbs" => [
                ['title' => "Factors", 'link' => "app_user_factors"],
                ['title' => "Orders for factor ".$factor->getFactorNumber(), 'link' => "#"],
            ],
            "factor" => $factor,
        ];

        return $this->render('pages/user/orders.html.twig', $items);
    }
}
