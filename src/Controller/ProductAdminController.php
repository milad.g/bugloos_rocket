<?php

namespace App\Controller;

use App\Entity\Product;
use App\Form\ProductFormType;
use App\Repository\ProductRepository;
use App\Service\UploadFileHelper;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ProductAdminController
 * @package App\Controller
 * @IsGranted("ROLE_ADMIN")
 */
class ProductAdminController extends AbstractController
{
    /**
     * @Route("/admin/product/index", name="admin_product_index")
     */
    public function index(Request $request, PaginatorInterface $paginator, ProductRepository $productRepository): Response
    {
        $q = $request->query->get('q');

        $pagination = $paginator->paginate(
            $productRepository->findLatestProductsWithQuery($q),
            $request->query->getInt('page', 1),
            10
        );

        $items = [
            "title" => "List of product",
            "breadcrumbs" => [
                ['title' => "List of product", 'link' => "admin_product_index"]
            ],
            'pagination' => $pagination
        ];

        return $this->render('admin/product/index.html.twig', $items);
    }

    /**
     * @Route("/admin/product/create", name="admin_product_create")
     */
    public function create(Request $request, EntityManagerInterface $entityManager, UploadFileHelper $uploadFileHelper): Response
    {
        $form = $this->createForm(ProductFormType::class);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            /** @var Product $product */
            $product = $form->getData();

            $imageFile = $form->get('imageFile')->getData();
            if ($imageFile) {
                $fileName = $uploadFileHelper->uploadFile($imageFile);
                $product->setImage($fileName);
            }

            $sliderFile = $form->get('sliderFile')->getData();
            if ($sliderFile) {
                $fileName = $uploadFileHelper->uploadFile($sliderFile);
                $product->setSlider($fileName);
            }

            $entityManager->persist($product);
            $entityManager->flush();

            $this->addFlash("success","Product added successfully");

            return $this->redirectToRoute("admin_product_index");
        }

        $items = [
            "title" => "Create product",
            "breadcrumbs" => [
                ['title' => "List of product", 'link' => "admin_product_index"],
                ['title' => "Create product", 'link' => "#"]
            ],
            'ProductForm' => $form->createView()
        ];

        return $this->render('admin/product/create.html.twig', $items);
    }

    /**
     * @Route("/admin/product/{id}/edit", name="admin_product_edit")
     */
    public function edit(Product $product, Request $request, EntityManagerInterface $entityManager, UploadFileHelper $uploadFileHelper): Response
    {
        $form = $this->createForm(ProductFormType::class, $product);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            $imageFile = $form->get('imageFile')->getData();
            if ($imageFile) {
                $fileName = $uploadFileHelper->uploadFile($imageFile);
                $product->setImage($fileName);
            }

            $sliderFile = $form->get('sliderFile')->getData();
            if ($sliderFile) {
                $fileName = $uploadFileHelper->uploadFile($sliderFile);
                $product->setSlider($fileName);
            }

            $entityManager->persist($product);
            $entityManager->flush();

            $this->addFlash("success","Product updated successfully");

            return $this->redirectToRoute("admin_product_edit",['id' => $product->getId()]);
        }

        $items = [
            "title" => "Edit product",
            "breadcrumbs" => [
                ['title' => "List of product", 'link' => "admin_product_index"],
                ['title' => "Edit product", 'link' => "#"]
            ],
            'ProductForm' => $form->createView()
        ];

        return $this->render('admin/product/edit.html.twig', $items);
    }
}
