<?php

namespace App\Controller;

use App\Entity\Category;
use App\Form\CategoryFormType;
use App\Repository\CategoryRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CategoryAdminController
 * @package App\Controller
 * @IsGranted("ROLE_ADMIN")
 */
class CategoryAdminController extends AbstractController
{
    /**
     * @Route("/admin/category/index", name="admin_category_index")
     */
    public function index(Request $request, CategoryRepository $categoryRepository, PaginatorInterface $paginator): Response
    {
        $q = $request->query->get('q');

        $pagination = $paginator->paginate(
            $categoryRepository->findLatestCategoriesWithQuery($q),
            $request->query->getInt('page', 1),
            10
        );

        $items = [
            "title" => "List of categories",
            "breadcrumbs" => [
                ['title' => "List of categories", 'link' => "admin_category_index"]
            ],
            'pagination' => $pagination
        ];

        return $this->render('admin/category/index.html.twig', $items);
    }

    /**
     * @Route("/admin/category/create", name="admin_category_create")
     */
    public function create(Request $request, EntityManagerInterface $entityManager)
    {
        $form = $this->createForm(CategoryFormType::class);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            /** @var Category $category */
            $category = $form->getData();

            $entityManager->persist($category);
            $entityManager->flush();

            $this->addFlash("success","Category added successfully");

            return $this->redirectToRoute("admin_category_index");
        }

        $items = [
            "title" => "Create category",
            "breadcrumbs" => [
                ['title' => "List of categories", 'link' => "admin_category_index"],
                ['title' => "Create category", 'link' => "#"]
            ],
            'CategoryForm' => $form->createView()
        ];

        return $this->render('admin/category/create.html.twig', $items);
    }

    /**
     * @Route("/admin/category/{id}/edit", name="admin_category_edit")
     */
    public function edit(Category $category, Request $request, EntityManagerInterface $entityManager)
    {
        $form = $this->createForm(CategoryFormType::class, $category);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $entityManager->persist($category);
            $entityManager->flush();

            $this->addFlash("success","Category updated successfully");

            return $this->redirectToRoute("admin_category_edit",['id' => $category->getId()]);
        }

        $items = [
            "title" => "Edit category",
            "breadcrumbs" => [
                ['title' => "List of categories", 'link' => "admin_category_index"],
                ['title' => "Edit category", 'link' => "#"]
            ],
            'CategoryForm' => $form->createView()
        ];

        return $this->render('admin/category/edit.html.twig', $items);
    }
}
