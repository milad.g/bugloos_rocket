<?php


namespace App\Message;


class AddToCart
{
    private $token;
    private $product;

    public function __construct($token, $product)
    {

        $this->token = $token;
        $this->product = $product;
    }

    public function getToken()
    {
        return $this->token;
    }

    public function getProduct()
    {
        return $this->product;
    }
}