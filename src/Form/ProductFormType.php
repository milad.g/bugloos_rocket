<?php

namespace App\Form;

use App\Entity\Category;
use App\Entity\Product;
use App\Form\DataTransformer\CommaPriceToPriceTransformer;
use App\Form\DataTransformer\RemoveDiscountPercentTransformer;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;

class ProductFormType extends AbstractType
{
    private $commaPriceToPriceTransformer;
    private $discountPercentTransformer;

    public function __construct(CommaPriceToPriceTransformer $commaPriceToPriceTransformer, RemoveDiscountPercentTransformer $discountPercentTransformer)
    {
        $this->commaPriceToPriceTransformer = $commaPriceToPriceTransformer;
        $this->discountPercentTransformer = $discountPercentTransformer;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('category', EntityType::class, [
                'class' => Category::class,
                'query_builder' => function(EntityRepository $er){
                    return $er->createQueryBuilder('c')->orderBy('c.title','ASC');
                },
                'choice_label' => 'title',
                'placeholder' => '-- choose a category --'
            ])
            ->add('title')
            ->add('price', TextType::class, [
                'attr' => ['class' => 'money price_value']
            ])
            ->add('discount', TextType::class, [
                'attr' => ['class' => 'percent discount_value'],
                'empty_data' => 0
            ])
            ->add('finalPrice', TextType::class, [
                'attr' => ['class' => 'money final_price_value']
            ])
            ->add('stock')
            ->add('description')
            ->add('imageFile', FileType::class,[
                'label' => 'Product image',
                'mapped' => false,
                'required' => false,
                'constraints' => [
                    new File([
                        'maxSize' => '1024k',
                        'maxSizeMessage' => "Maximum upload size is 1M",
                        'mimeTypes' => [
                            'image/jpeg',
                            'image/png'
                        ],
                        'mimeTypesMessage' => 'You can only upload image with img or png format'
                    ])
                ]
            ])
            ->add('sliderFile', FileType::class,[
                'label' => 'Slider image',
                'mapped' => false,
                'required' => false,
                'constraints' => [
                    new File([
                        'maxSize' => '2048k',
                        'maxSizeMessage' => "Maximum upload size is 2M",
                        'mimeTypes' => [
                            'image/jpeg',
                            'image/png'
                        ],
                        'mimeTypesMessage' => 'You can only upload image with img or png format'
                    ])
                ]
            ])
        ;

        $builder->get('price')->addModelTransformer($this->commaPriceToPriceTransformer);
        $builder->get('finalPrice')->addModelTransformer($this->commaPriceToPriceTransformer);
        $builder->get('discount')->addModelTransformer($this->discountPercentTransformer);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Product::class,
        ]);
    }
}
