<?php


namespace App\MessageHandler;


use App\Entity\Order;
use App\Message\AddToCart;
use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

class AddToCartHandler implements MessageHandlerInterface
{
    private $csrfTokenManager;
    private $productRepository;
    private $entityManager;
    private $security;

    public function __construct(CsrfTokenManagerInterface $csrfTokenManager, ProductRepository $productRepository, EntityManagerInterface $entityManager, Security $security)
    {

        $this->csrfTokenManager = $csrfTokenManager;
        $this->productRepository = $productRepository;
        $this->entityManager = $entityManager;
        $this->security = $security;
    }

    public function __invoke(AddToCart $addToCart)
    {
        $token = $addToCart->getToken();
        $product = $addToCart->getProduct();


        $token = new CsrfToken('add-to-cart', $token);
        if (!$this->csrfTokenManager->isTokenValid($token)) {
            throw new InvalidCsrfTokenException();
        }

        $product = $this->productRepository->findOneBy(['id' => $product]);
        if(!$product){
            throw new NotFoundHttpException("This product is not available");
        }

        $order = new Order();
        $order->setPrice($product->getPrice());
        $order->setDiscount($product->getDiscount());
        $order->setFinalPrice($product->getFinalPrice());
        $order->setProduct($product);
        $order->setUser($this->security->getUser());

        $this->entityManager->persist($order);
        $this->entityManager->flush();
    }
}