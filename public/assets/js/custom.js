// NUMBER FORMAT
function number_format(number){
    let cleanNumber = number.toString().replace(/[^\d]/g, '');
    if(cleanNumber.length > 3)
        cleanNumber = cleanNumber.replace(/\B(?=(?:\d{3})+(?!\d))/g, ',');
    return cleanNumber;
}

// NUMBER UN FORMAT
function number_un_format(number){
    return number.replace(/,/g, '');
}

// round_num
function round_number(number){
    return Math.round(number/100)*100;
}

$(document).ready(function () {
    //price discount
    function price_discount(){
        let $price = $(".price_value");
        let $discount = $(".discount_value");

        let price = number_un_format($price.val());
        console.log(price);
        let discount = ($discount.val().trim() !== '') ? $discount.val().trim() : 0;
        discount = parseFloat(discount).toFixed(1);
        let discount_val = (price * discount) / 100;
        let price_discount = number_format(round_number(price - discount_val));
        $(".final_price_value").val(price_discount);
    }

    $(".price_value").on('keyup', function() {
        price_discount();
    });

    $(".discount_value").on('keyup', function() {
        price_discount();
    });
    //End price discount
})
